import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./App.css";
import firebase from "./Firebase";
import pt from 'date-fns/locale/pt'
import { registerLocale, setDefaultLocale } from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

registerLocale('pt', pt);

class App extends Component {
  constructor(props) {
    super(props);
    this.ref = firebase.firestore().collection("events");
    this.unsubscribe = null;
    this.state = {
      events: []
    };
  }

  onCollectionUpdate = querySnapshot => {
    const events = [];
    querySnapshot.forEach(event => {
      let { name, description, eventDate } = event.data();
      eventDate = new Date(eventDate.seconds * 1000).toLocaleDateString();
      events.push({
        key: event.id,
        event,
        name,
        description,
        eventDate
      });
    });
    this.setState({
      events
    });
  };

  componentDidMount() {
    this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
  }

  render() {
    return (
      <div className="jumbotron jumbotron-fluid">
        <div className="card">
          <div className="container">
            <div className="panel panel-default">
              <div className="panel-heading">
                <h3 className="panel-name">BTicket - Eventos</h3>
              </div>
              <br />
              <div className="panel-body">
                <h4>
                  <Link className="btn btn-primary" to="/create">
                    Novo +
                  </Link>
                </h4>
                <br />
                <table className="table table-stripe">
                  <thead>
                    <tr>
                      <th>Nome</th>
                      <th>Descrição</th>
                      <th>Data</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.events.map(event => (
                      <tr>
                        <td><Link to={`/show/${event.key}`}>{event.name}</Link></td>
                        <td>{event.description}</td>
                        <td>{event.eventDate}</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
