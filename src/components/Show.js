import React, { Component } from "react";
import firebase from "../Firebase";
import { Link } from "react-router-dom";

class Show extends Component {
  constructor(props) {
    super(props);
    this.state = {
      event: {
        ticketTypes: []
      },
      key: ""
    };
  }

  componentDidMount() {
    const ref = firebase
      .firestore()
      .collection("events")
      .doc(this.props.match.params.id);
    ref.get().then(doc => {
      if (doc.exists) {
        let event = doc.data();
        event.eventDate = new Date(
          event.eventDate.seconds * 1000
        ).toLocaleDateString();
        this.setState({
          event: event,
          key: doc.id,
          isLoading: false
        });
      }
    });
  }

  render() {
    return (
      <div className="jumbotron jumbotron-fluid">
        <div className="card">
          <div class="container">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">{this.state.event.name}</h3>
              </div>
              <div class="panel-body">
                <dl>
                  <div className="card-body">
                    <dt>Descrição do evento:</dt>
                    <dd>{this.state.event.description}</dd>
                  </div>
                  <div className="card-body">
                    <dt>Data do evento:</dt>
                    <dd>{this.state.event.eventDate}</dd>
                  </div>
                  <div className="card-body">
                    <table className="table table-stripe">
                      <thead>
                        <tr>
                          <th>Tipo de ingresso</th>
                          <th>Valor</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.state.event.ticketTypes.map(tickettype => (
                          <tr>
                            <td>{tickettype.name}</td>
                            <td>{tickettype.value}</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </dl>
                <div class="panel-footer">
                  <h4>
                    <Link className="btn btn-primary" to="/">
                      Voltar
                    </Link>
                  </h4>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Show;
