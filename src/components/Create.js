import React, { Component } from "react";
import firebase from "../Firebase";
import { Link } from "react-router-dom";
import DatePicker from "react-datepicker";
import pt from "date-fns/locale/pt";
import { registerLocale, setDefaultLocale } from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

registerLocale("pt", pt);

class Create extends Component {
  constructor() {
    super();
    this.ref = firebase.firestore().collection("events");
    this.state = {
      name: "",
      description: "",
      eventDate: new Date(),
      ticketTypes: []
    };
  }
  onChange = e => {
    const state = this.state;
    state[e.target.name] = e.target.value;
    this.setState(state);
  };

  onDateChange = date => {
    const state = this.state;
    state["eventDate"] = date;
    this.setState(state);
  };

  handleAddEvent = evt => {
    var id = (+new Date() + Math.floor(Math.random() * 999999)).toString(36);
    var ticketType = {
      id: id,
      name: "",
      value: ""
    };
    this.state.ticketTypes.push(ticketType);
    this.setState(this.state.ticketTypes);
  };

  handleTicketTypeTable = evt => {
    var item = {
      id: evt.target.id,
      name: evt.target.name,
      value: evt.target.value
    };
    var ticketTypes = this.state.ticketTypes.slice();
    var ticketType = ticketTypes.map(function(ticket) {
      for (var key in ticket) {
        if (key == item.name && ticket.id == item.id) {
          ticket[key] = item.value;
        }
      }
      return ticket;
    });
    this.setState({ ticketTypes: ticketType });
  };

  onSubmit = e => {
    e.preventDefault();
    const { name, description, eventDate, ticketTypes } = this.state;
    this.ref
      .add({
        name,
        description,
        eventDate, 
        ticketTypes
      })
      .then(docRef => {
        this.setState({
          name: "",
          description: "",
          eventDate: "", 
          ticketTypes: []
        });
        this.props.history.push("/");
      })
      .catch(error => {
        console.error("Error adding event: ", error);
      });
  };

  render() {
    const { name, description, eventDate, ticketTypes } = this.state;
    return (
      <div className="container">
        <div className="panel panel-default">
          <br />
          <div className="panel-heading">
            <h3 className="panel-name">Novo evento</h3>
          </div>
          <div className="panel-body">
            <br />
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <label htmlFor="name">Nome*</label>
                <input
                  type="text"
                  className="form-control"
                  name="name"
                  value={name}
                  onChange={this.onChange}
                  placeholder="Digite o nome do evento"
                  required
                />
              </div>
              <div className="form-group">
                <label htmlFor="description">Descrição*</label>
                <textarea
                  className="form-control"
                  name="description"
                  onChange={this.onChange}
                  placeholder="Digite a descrição do evento"
                  cols="80"
                  rows="3"
                  value={description}
                  required
                />
              </div>
              <div className="form-group">
                <label htmlFor="eventDate">Data do evento* </label>
                <div className="form-group">
                  <DatePicker
                    selected={eventDate}
                    onChange={this.onDateChange}
                    locale="pt"
                    dateFormat="dd/MM/yyyy"
                  />
                </div>
              </div>

              <div className="form-group">
                <div className="card card-body">
                  <label htmlFor="eventDate">Tipos de ingressos* </label>
                  <div>
                    <TicketTypeTable
                      onTicketTypesTableUpdate={this.handleTicketTypeTable.bind(
                        this
                      )}
                      onRowAdd={this.handleAddEvent.bind(this)}
                      ticketTypes={ticketTypes}
                    />
                  </div>
                </div>
              </div>
              <br />
              <div>
                <h4>
                  <Link to="/" className="float-right btn btn-primary">
                    Voltar
                  </Link>
                </h4>
              </div>
              <button type="submit" className="btn btn-success">
                Salvar
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

class TicketTypeTable extends React.Component {
  render() {
    var onTicketTypesTableUpdate = this.props.onTicketTypesTableUpdate;
    var ticketType = this.props.ticketTypes.map(function(ticketType) {
      return (
        <TicketTypeRow
          onTicketTypesTableUpdate={onTicketTypesTableUpdate}
          ticketType={ticketType}
          key={ticketType.id}
        />
      );
    });
    return (
      <div>
        <button
          type="button"
          onClick={this.props.onRowAdd}
          className="btn btn-secondary float-right"
        >
         Adicionar +
        </button>
        <table className="table table-bordered">
          <thead>
            <tr>
              <th>Tipo</th>
              <th>Valor</th>
            </tr>
          </thead>
          <tbody>{ticketType}</tbody>
        </table>
      </div>
    );
  }
}

class TicketTypeRow extends React.Component {
  render() {
    return (
      <tr className="eachRow">
        <EditableCell
          onTicketTypesTableUpdate={this.props.onTicketTypesTableUpdate}
          cellData={{
            type: "name",
            format: "text",
            placeholder: "Informe o tipo de ingresso",
            value: this.props.ticketType.name,
            id: this.props.ticketType.id
          }}
        />
        <EditableCell
          onTicketTypesTableUpdate={this.props.onTicketTypesTableUpdate}
          cellData={{
            type: "value",
            format: "number",
            placeholder: "Informe o valor",
            value: this.props.ticketType.value,
            id: this.props.ticketType.id
          }}
        />
      </tr>
    );
  }
}

class EditableCell extends React.Component {
  render() {
    return (
      <td>
        <input
          className="form-control"
          type={this.props.cellData.format}
          placeholder={this.props.cellData.placeholder}
          name={this.props.cellData.type}
          id={this.props.cellData.id}
          value={this.props.cellData.value}
          onChange={this.props.onTicketTypesTableUpdate}
          required
        />
      </td>
    );
  }
}

export default Create;
