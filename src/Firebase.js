import * as firebase from "firebase";
import firestore from "firebase/firestore";

const settings = { timestampsInSnapshots: true };

const config = {
  apiKey: "AIzaSyBocKqMbwMIWX8XiUZ11FW9mK0pqn-6lf4",
  authDomain: "bticketevent.firebaseapp.com",
  databaseURL: "https://bticketevent.firebaseio.com",
  projectId: "bticketevent",
  storageBucket: "bticketevent.appspot.com",
  messagingSenderId: "128603698364"
};
firebase.initializeApp(config);

firebase.firestore().settings(settings);

export default firebase;
